import axios from 'axios'
import config from '../../config.js'

export function GET_POSTS (context, params) {
  context.commit('SET_LOADING', { flag: true, key: 'postLoading' })
  context.commit('RESET_DETAILS_VIEW', true)

  return new Promise((resolve, reject) => {
    let url = config.posts

    if (params) {
      url = `${config.posts}?${params.type}=${params.value}`
    }

    axios.get(url)
      .then((resp) => {
        context.commit('SET_POSTS', resp.data)
        resolve(resp)
      }).catch((error) => {
        reject(error)
      }).finally((_) => context.commit('SET_LOADING', { flag: false, key: 'postLoading' }))
  })
}

export function LOAD_MORE_POSTS (context, params) {
  context.commit('SET_LOADING', { flag: true, key: 'oldPostLoading' })

  return new Promise((resolve, reject) => {
    const nextPage = context.state.postMeta.next_page
    let url = `${config.getOldPosts}?page_handle=${nextPage}`

    if (params) {
      url = `${url}&${params.type}=${params.value}`
    }

    axios.get(url)
      .then((resp) => {
        context.commit('APPEND_POSTS', resp.data)
        resolve(resp)
      }).catch((error) => {
        reject(error)
      }).finally((_) => context.commit('SET_LOADING', { flag: false, key: 'oldPostLoading' }))
  })
}

export function GET_TAGS (context) {
  context.commit('SET_LOADING', { flag: true, key: 'tagsLoading' })

  return new Promise((resolve, reject) => {
    axios.get(config.tags)
      .then((resp) => {
        context.commit('SET_TAGS', resp.data)
        resolve(resp)
      }).catch((error) => {
        reject(error)
      }).finally((_) => context.commit('SET_LOADING', { flag: false, key: 'tagsLoading' }))
  })
}

export function GET_CATEGORIES (context) {
  context.commit('SET_LOADING', { flag: true, key: 'catLoading' })

  return new Promise((resolve, reject) => {
    axios.get(config.categories)
      .then((resp) => {
        context.commit('SET_CATEGORIES', resp.data)
        resolve(resp)
      }).catch((error) => {
        reject(error)
      }).finally((_) => context.commit('SET_LOADING', { flag: false, key: 'catLoading' }))
  })
}

export function GET_POST_BY_ID (context, postId) {
  context.commit('SET_LOADING', { flag: true, key: 'postLoading' })
  context.commit('RESET_DETAILS_VIEW', true)

  return new Promise((resolve, reject) => {
    axios.get(`${config.postById}?post_id=${postId}`)
      .then((resp) => {
        context.commit('SET_SINGLE_POST', resp.data)
        resolve(resp)
      }).catch((error) => {
        reject(error)
      }).finally((_) => context.commit('SET_LOADING', { flag: false, key: 'postLoading' }))
  })
}
