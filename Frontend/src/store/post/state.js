export default function () {
  return {
    posts: [],
    postMeta: {},
    categories: [],
    tags: [],
    postDetails: {},
    postLoading: false,
    tagsLoading: false,
    catLoading: false,
    oldPostLoading: false
  }
}
