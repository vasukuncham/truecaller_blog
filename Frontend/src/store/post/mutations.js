export function SET_POSTS (state, data) {
  if (data.posts.length > 0) {
    const posts = data.posts.map(el => {
      return {
        id: el.ID,
        title: el.title,
        author: el?.author ? el.author.name : '',
        date: el.date,
        excerpt: el.excerpt,
        image: el.featured_image
      }
    })

    state.posts = posts
    state.postMeta = data.meta
  } else {
    state.posts = []
    state.postMeta = {}
  }
}

export function APPEND_POSTS (state, data) {
  if (data.posts.length > 0) {
    const posts = data.posts.map(el => {
      return {
        id: el.ID,
        title: el.title,
        author: el?.author ? el.author.name : '',
        date: el.date,
        excerpt: el.excerpt,
        image: el.featured_image
      }
    })

    state.posts = [...state.posts, ...posts]
    state.postMeta = data.meta
  } else {
    state.postMeta = {}
  }
}

export function SET_TAGS (state, data) {
  if (data.tags.length > 0) {
    const tags = data.tags.map(el => {
      return {
        id: el.ID,
        name: el.name,
        slug: el.slug,
        post_count: el.post_count
      }
    })
    state.tags = tags
  } else {
    state.tags = []
  }
}

export function SET_CATEGORIES (state, data) {
  if (data.categories.length > 0) {
    const categories = data.categories.map(el => {
      return {
        id: el.ID,
        name: el.name,
        slug: el.slug,
        post_count: el.post_count
      }
    })
    state.categories = categories
  } else {
    state.categories = []
  }
}

export function SET_SINGLE_POST (state, data) {
  const { ID, title, author, date, excerpt, content } = data
  state.postDetails = {
    title,
    date,
    excerpt,
    content,
    id: ID,
    author: author?.name,
    image: data.featured_image
  }
}

export function SET_LOADING (state, { flag, key }) {
  state[key] = flag
}

export function RESET_DETAILS_VIEW (state) {
  state.postDetails = {}
}
