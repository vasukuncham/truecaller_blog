import moment from 'moment'

export const DATE_FORMAT = (date) => {
  return moment(date).fromNow()
}
