export const domain = 'http://localhost:3000/'

export default {
  posts: domain + 'posts/',
  tags: domain + 'tags/',
  categories: domain + 'categories/',
  postById: domain + 'postById/',
  getOldPosts: domain + 'getOldPosts/'
}
