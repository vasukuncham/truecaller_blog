
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'view/:id', component: () => import('pages/Details.vue') },
      { path: 'tags-view/:slug', component: () => import('pages/TagsView.vue') },
      { path: 'categories-view/:slug', component: () => import('pages/CategoriesView.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
