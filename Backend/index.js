const express = require('express');
const axios = require('axios');
const cors = require('cors');

const app = express();
const port = 3000;

app.use(cors());

const siteID = '107403796';
const blogUri = `https://public-api.wordpress.com/rest/v1.1/sites/${siteID}/`
const fields = 'ID,title,author,date,excerpt,featured_image,content';

app.get('/posts', async (req, res) => {
  try {
    let url = `${blogUri}posts/?fields=${fields}`;
    if (req.query.tag) {
      url = `${url}&tag=${req.query.tag}`;
    }
    if (req.query.category) {
      url = `${url}&category=${req.query.category}`;
    }
    const resp = await axios.get(url);
    res.send(resp.data)
  } catch (err) {
    res.send({found: 0, posts: []})
  }
})

app.get('/getOldPosts', async (req, res) => {
  try {
    let url = `${blogUri}posts/?fields=${fields}&page_handle=${req.query.page_handle}`;
    if (req.query.tag) {
      url = `${url}&tag=${req.query.tag}`;
    }
    if (req.query.category) {
      url = `${url}&category=${req.query.category}`;
    }
    const resp = await axios.get(url);
    res.send(resp.data)
  } catch (err) {
    res.send({found: 0, posts: []})
  }
})

app.get('/postById', async (req, res) => {
  try {
    const resp = await axios.get(`${blogUri}posts/${req.query.post_id}/?fields=${fields}`);
    res.send(resp.data)
  } catch (err) {
    res.send({found: 0, posts: []})
  }
})

app.get('/relatedPosts', async (req, res) => {
  try {
    const resp = await axios.post(`${blogUri}posts/${req.query.post_id}/related/`, {size: 3});
    res.send(resp.data)
  } catch (err) {
    res.send({found: 0, posts: []})
  }
})

app.get('/categories', async (req, res) => {
  try {
    const resp = await axios.get(`${blogUri}categories/`);
    res.send(resp.data)
  } catch (err) {
    res.send({found: 0, posts: []})
  }
})

app.get('/tags', async (req, res) => {
  try {
    const resp = await axios.get(`${blogUri}tags/?number=15&order_by=count&order=DESC`);
    res.send(resp.data)
  } catch (err) {
    res.send({found: 0, posts: []})
  }
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})






